**Assignment 08**


Write down a program to store the details of students using the knowledge of loops, arrays and structures you gained so far. First name of the student, subject and its marks for each student needs to be recorded. Data should be fetched through user input (do not hard code) and print the data back. Program should record and display information of minimum 5 students.

Hint: use array to hold structures and use loops to access structures in the array
