#include <stdio.h>
#define SIZE 25

struct student_info
{
    char sname[SIZE];
    char subject[SIZE];
    int mark;
};

int main()
{
    int i,x;
    printf("Enter the number of students : ");
    scanf("%d",&x);

    struct student_info info[SIZE];
    printf("\n\nEnter the Following Details.\n");
    printf("_____________________________\n\n");

    for(i=1; i<=x; i++)
    {
        printf("Enter First Name of Student : ");
        scanf("%s", info[i].sname);
        printf("Enter the Subject : ");
        scanf("%s", info[i].subject);
        printf("Enter Marks : ");
        scanf("%d", &(info[i].mark));
        printf("\n");
    }

    printf("\n\nSummery of entered Details.\n");
    printf("_____________________________\n\n");
    printf("\nName\t\tSubject\t\tMarks\t\n\n");

    for(i=1; i<=x; i++)
    {
        printf("%s\t\t%s\t\t%d\t\n", info[i].sname,info[i].subject,info[i].mark);
    }

    return 0;
}
